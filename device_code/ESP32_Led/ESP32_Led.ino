#include <stdint.h>
#include <WiFi.h>
#include <FS.h>
#include <SPIFFS.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>

const char* ssid     = "VojtovoWiFi";
const char* password = "lejnozkapra";

AsyncWebServer server(80);

void setup() {
  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  serverDefine();
  //server.serveStatic("/", SPIFFS, "/").setDefaultFile("index.html");

  server.begin();
  pinMode(23,OUTPUT);

}





void loop() {

}




void serverDefine() {                                                                       // DEFINE SERVERS
  server.on("/on", HTTP_GET, [](AsyncWebServerRequest * request) {
    Serial.println("Request: On");
    digitalWrite(23,HIGH);
    request->send(200, "text/plain", "Ok");
  });
  server.on("/off", HTTP_GET, [](AsyncWebServerRequest * request) {
    Serial.println("Request: Off");
    digitalWrite(23,LOW);
    request->send(200, "text/plain", "Ok");
  });
  server.onNotFound(notFound);
}


void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}
