from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class RssFeed(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE )
    url = models.CharField(max_length = 200)
    def __str__(self):
        return (str(self.user) + " - " + self.url)

class RssFeedRecommend(models.Model):
    url = models.CharField(max_length = 200)
    name = models.CharField(max_length=50)
    info = models.CharField(max_length=100)
    def __str__(self):
        return self.name

