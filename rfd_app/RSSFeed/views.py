from django.shortcuts import render, redirect
from .models import RssFeed, RssFeedRecommend
from django.contrib.auth.models import User
import feedparser
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.core import serializers
from django.http import HttpResponse
from accounts.models import Setting
from django.core.files import File

# Create your views here.

@login_required(login_url='/accounts/login/')
def addRssFeed(request):
    if request.method == 'POST':
        data = RssFeed(url=request.POST['url'],user = User.objects.get(username = request.user.get_username()))
        data.save()
        return HttpResponseRedirect('/')
    Recommended_list = []
    Tables = RssFeedRecommend.objects.all()
    UrlTables = RssFeed.objects.filter(user=User.objects.get(username = request.user.get_username()))
    for RecommendedTable in Tables:
        Recommended_list.append(RecommendedTable)
    for Table in UrlTables:
        for RecommendedTable in Tables:
            if(Table.url == RecommendedTable.url):
                Recommended_list.remove(RecommendedTable)

    json = serializers.serialize('json', Recommended_list)
    context = {'json':json}
    return render(request, 'add_rss_feed.html',context)

@login_required(login_url='/accounts/login/')
def RemoveRss(request):
    if request.method == 'POST':
        RssFeed.objects.filter(user=User.objects.get(username = request.user.get_username()), url=request.POST['url']).delete()
        return redirect('Accounts:settings')
    
    UrlTables = RssFeed.objects.filter(user=User.objects.get(username = request.user.get_username()))
    RSSList = []
    for Rss in UrlTables:
        RSSList.append(Rss)
    context = {'RSSList':RSSList}
    return render(request, 'remove_rss.html', context)


@login_required(login_url='/accounts/login/')
def feed(request):
    #Jsem buh, po pul roce to zapnout a bejt to během hodinky schopnej napsat ... 
    feeds = []
    settings = Setting.objects.filter(user=User.objects.get(username = request.user.get_username()))
    for setting in settings:
        number = setting.number_of_posts
    UrlTables = RssFeed.objects.filter(user=User.objects.get(username = request.user.get_username()))
    for Table in UrlTables:
        feeds.append(feedparser.parse(Table.url))
    #feeds = feedparser.parse('http://feeds.feedburner.com/respekt-clanky')
    context = {'feeds':feeds, 'number':number}
    return render(request, 'feed.html', context)
