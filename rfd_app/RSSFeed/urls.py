from django.conf.urls import url   
from . import views
app_name = 'RSSFeed'

urlpatterns = [
    url(r'^add_rss_feed/$', views.addRssFeed, name='add_rss_feed'),
    url(r'^remove_rss/$', views.RemoveRss, name='remove_rss'),
    url(r'^feed/$', views.feed, name='feed'),
    url('',views.feed, name='feed')
    



]