from django.contrib import admin

from .models import RssFeed, RssFeedRecommend
admin.site.register(RssFeed)
admin.site.register(RssFeedRecommend)


# Register your models here.
