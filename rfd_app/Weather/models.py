from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Weather(models.Model):
    location= models.CharField(max_length = 50)
    lon = models.CharField(max_length=50)
    lat = models.CharField(max_length=50)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.user) + " - " + self.location

class Unit(models.Model):
    unit = models.CharField(max_length = 10)

    def __str__(self):
        return self.unit