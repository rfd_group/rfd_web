from django.conf.urls import url   
from . import views
app_name = 'Weather'

urlpatterns = [
    url(r'^add_weather/$', views.addWeather, name='add_weather'),
    url(r'^weather/$', views.WeatherAll, name='weather'),
    url('',views.WeatherAll, name='weather')



]