from django.shortcuts import render
from .models import Weather, Unit
from accounts.models import Setting
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required(login_url='/accounts/login/')
def addWeather(request):
    if request.method == 'POST':
        if len(Weather.objects.filter(user = User.objects.get(username = request.user.get_username()))) > 0:
            Weather_data = Weather.objects.get(user = User.objects.get(username = request.user.get_username()))
            Weather_data.location = request.POST['location']
            Weather_data.lon = request.POST['lon']
            Weather_data.lat = request.POST['lat']
            Weather_data.save()
        else:
            data = Weather(location=request.POST['location'],lon=request.POST['lon'],lat=request.POST['lat'],user = User.objects.get(username = request.user.get_username()))
            data.save()
        return HttpResponseRedirect('/')
    else:
        return render(request, 'add_weather.html')
   
@login_required(login_url='/accounts/login/')
def WeatherAll(request):
    setting_list = Setting.objects.filter(user=User.objects.get(username = request.user.get_username()))
    for setting in setting_list:
        if setting.units ==  Unit.objects.get(unit='°C + km/h'):
            unit = 'ca'
        elif setting.units == Unit.objects.get(unit='°F + mp/h') :
            unit = 'us'
        else:
            unit='ca'
    pocasi_list = Weather.objects.filter(user=User.objects.get(username = request.user.get_username()))
    context = {'pocasi_list':pocasi_list, 'unit':unit}
    return render(request, 'Weather.html', context)
