from django.contrib import admin
from .models import Weather, Unit

admin.site.register(Weather)
admin.site.register(Unit)


# Register your models here.
