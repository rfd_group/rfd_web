from django.conf.urls import url   
from . import views
app_name = 'Accounts'

urlpatterns = [
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^login/$', views.loginV, name='login'),
    url(r'^logout/$',views.logoutV, name='logout'),
    url(r'^change_password/$',views.changePassword, name='change_password'),
    url(r'^edit_profile/$',views.editProfile, name='edit_profile'),
    url(r'^profile/$',views.profile, name='profile'),
    url(r'^settings/$',views.settings, name='settings'),
    url('',views.profile, name='profile')

    



]