from django.db import models
from django.contrib.auth.models import User
from Weather.models import Unit

# Create your models here.
class Setting(models.Model):
    number_of_posts = models.PositiveIntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    units = models.ForeignKey(Unit, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.user.username
