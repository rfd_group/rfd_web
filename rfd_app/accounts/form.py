from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, ReadOnlyPasswordHashField, UsernameField
from django.contrib.auth.models import User
from django.utils.translation import gettext, gettext_lazy as _



class RegistForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True, help_text='')
    last_name = forms.CharField(max_length=30, required=True, help_text='')
    email = forms.EmailField(max_length=254, required=True, help_text='')

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'password1', 'password2', )

    def save(self, commit=True):
        user = super(RegistForm, self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        return user

class EditProfileForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('email','first_name','last_name' )
        field_classes = {'username': UsernameField}

    def clean_password(self):
        return self.initial.get('password')
