from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, PasswordChangeForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import update_session_auth_hash, login, logout
from django.contrib.auth.models import User
from .models import Setting
from Weather.models import Unit
from accounts.form import RegistForm, EditProfileForm
# Create your views here.
def signup(request):
    if request.method == 'POST':
        form = RegistForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            data = Setting(number_of_posts=5,user = User.objects.get(username = request.user.get_username()), units= Unit.objects.get(unit='°C + km/h'))
            data.save()
            return redirect('rfd_web:index')
    else:
        form = RegistForm()
    return render(request,'signup.html', {'form': form})

def loginV(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('rfd_web:index')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})

def logoutV(request):
    if request.method == 'POST':
        logout(request)
        return redirect('Accounts:login')

@login_required(login_url='/accounts/login/')
def changePassword(request):
    if request.method =='POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            return redirect('/')
    else:
        form = PasswordChangeForm(request.user)
    return render(request,'change_password.html', {'form':form})

@login_required(login_url='/accounts/login/')
def editProfile(request):
    if request.method == 'POST':
        form=EditProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('rfd_web:index')

    form = EditProfileForm(instance=request.user)
    context = {'form':form}
    return render(request, 'edit_profile.html', context)

@login_required(login_url='/accounts/login/')
def profile(request):
    return render(request, 'profile.html')

@login_required(login_url='/accounts/login/')
def settings(request):
    if request.method == 'POST':
        #Setting.objects.filter(user = User.objects.get(username = request.user.get_username())).delete()
        Setting_data = Setting.objects.get(user = User.objects.get(username = request.user.get_username()))
        #data = Setting(number_of_posts=request.POST['number_of_posts'],units=Unit.objects.get(unit=request.POST['units']),user=User.objects.get(username = request.user.get_username()))
        #data.save()
        #To přece nebudu kvůli editu mazat FailFish
        Setting_data.number_of_posts = request.POST['number_of_posts']
        Setting_data.units = Unit.objects.get(unit=request.POST['units'])
        Setting_data.save()
        return redirect('Accounts:settings')
    Settings = Setting.objects.filter(user=User.objects.get(username = request.user.get_username()))
    Units = Unit.objects.exclude(unit=Setting.objects.get(user=User.objects.get(username = request.user.get_username())).units)
    context = {'Settings':Settings, 'Units':Units}
    return render(request, 'settings.html', context)