from django.apps import AppConfig


class RfdWebConfig(AppConfig):
    name = 'rfd_web'
