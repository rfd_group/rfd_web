from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .models import SmartDevice, DeviceType
from RSSFeed.models import RssFeed
from Weather.models import Weather, Unit
from accounts.models import Setting
import urllib.request
import feedparser
import json
# Create your views here.

def index(request):
    if request.user.is_authenticated:
        setting_list = Setting.objects.filter(user=User.objects.get(username = request.user.get_username()))
        for setting in setting_list:
            if setting.units ==  Unit.objects.get(unit='°C + km/h'):
                unit = 'ca'
            elif setting.units == Unit.objects.get(unit='°F + mp/h'):
                unit = 'us'
            else:
                unit='ca'
        device_list = SmartDevice.objects.all()
        pocasi_list = Weather.objects.filter(user=User.objects.get(username = request.user.get_username()))
        feeds = []
        UrlTables = RssFeed.objects.filter(user=User.objects.get(username = request.user.get_username()))
        for Table in UrlTables:
            feeds.append(feedparser.parse(Table.url))
        context = {'device_list': device_list, 'pocasi_list':pocasi_list,'feeds':feeds, 'unit':unit}
        return render(request, 'dashboard.html', context)
    else:
        device_list = SmartDevice.objects.all()
        context = {'device_list': device_list}
        return render(request, 'dashboard_unlogged.html', context)

def removeDevice(request):
    if request.method == 'POST':
        SmartDevice.objects.filter(id=request.POST['id']).delete()
        return redirect('Accounts:settings')
    device_list = SmartDevice.objects.all()
    context = {'device_list':device_list}
    return render(request, 'remove_device.html', context)

@login_required(login_url='/accounts/login/')
def addDevice(request):
    if request.method == 'POST':
        data = SmartDevice(device_type=DeviceType.objects.get(id=request.POST['device_type']),name=request.POST['name'],ip=request.POST['ip'])
        data.save()
        return HttpResponseRedirect('/')
    device_list = DeviceType.objects.all()
    context = {'device_list': device_list}
    return render(request, 'add_device.html', context)


def deviceDetail(request, device_id):
    device = get_object_or_404(SmartDevice, pk=device_id)
    return render(request, 'device_detail.html', {'device': device})

def deviceAction(request, device_id, action):
    device = get_object_or_404(SmartDevice, pk=device_id)
    url = "http://" + device.ip + "/" + action
    urllib.request.urlopen(url)
    return redirect('rfd_web:device_detail', device_id = device.id)
    #return render(request, 'device_detail.html', {'device': device})

def deviceFeed(request):
    device_list = SmartDevice.objects.all()
    context = {'device_list': device_list}
    return render(request, 'device_feed.html', context)