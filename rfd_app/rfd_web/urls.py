from django.urls import path
from . import views
app_name = 'rfd_web'

urlpatterns = [
    path('', views.index, name='index'),
    path('add_device', views.addDevice, name='add_device'),
    path('device_detail/<int:device_id>/', views.deviceDetail, name='device_detail'),
    path('device_action/<int:device_id>/<path:action>', views.deviceAction, name='device_detail'),
    path('device_feed', views.deviceFeed, name='device_feed'),
    path('remove_device/',views.removeDevice, name='remove_device'),

]