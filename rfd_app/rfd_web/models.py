from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class DeviceType(models.Model):
    name = models.CharField(max_length = 50)
    def __str__(self):
        return self.name

class SmartDevice(models.Model):
    device_type = models.ForeignKey(DeviceType, on_delete=models.CASCADE)
    name = models.CharField(max_length = 50)
    ip = models.GenericIPAddressField()
    def __str__(self):
        return self.name

