from django.contrib import admin

from .models import DeviceType, SmartDevice

# Register your models here.

admin.site.register(DeviceType)
admin.site.register(SmartDevice)

